// Package p contains an HTTP Cloud Function.
package trigger_detect

import (
	"context"
	"encoding/json"
	"fmt"
	"html"
	"io"
	"log"
	"net/http"
	"os"

	"cloud.google.com/go/pubsub"
)

// GOOGLE_CLOUD_PROJECT is a user-set environment variable.
var projectID = os.Getenv("PROJECT_ID")

// client is a global Pub/Sub client, initialized once per instance.
var client *pubsub.Client
const HornetDetectedTopic = "HORNET_DETECTED"
const NestDetectedTopic = "HORNET_DETECTED"

func initPubSubClient() {
	log.Printf("initPubSubClient called, %v", projectID)
	// err is pre-declared to avoid shadowing client.
	var err error

	// client is initialized with context.Background() because it should
	// persist between function invocations.
	client, err = pubsub.NewClient(context.Background(), projectID)
	if err != nil {
		log.Fatalf("pubsub.NewClient: %v", err)
	}
}

func jsonEncode(detected Detected) []byte {
	var jsonData, err = json.Marshal(detected)
	if err != nil {
		log.Fatal("Unable to decode", err)
	}
	return jsonData
}

type Detected struct {
	DetectedType string `json:"detectedType"`
	LatLng       struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"LatLng"`
}

func getTopic(detectedType string) string {
	switch detectedType {
	case "HORNET":
		return HornetDetectedTopic
	case "NEST":
		return NestDetectedTopic
	default:
		log.Fatal("Non valid detectedType", detectedType)
		return ""
	}

}

func handleDecodeError(err error, responseWriter http.ResponseWriter) {
	switch err {
	case io.EOF:
		fmt.Fprint(responseWriter, "Nothing provided!")
		http.Error(responseWriter, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
	default:
		log.Printf("json.NewDecoder: %v", err)
		http.Error(responseWriter, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
	}
	log.Fatal("Unable to decode", err)
}

func Detect(responseWriter http.ResponseWriter, request *http.Request) {
	var detected = Detected{}

	initPubSubClient()

	if err := json.NewDecoder(request.Body).Decode(&detected); err != nil {
		handleDecodeError(err, responseWriter)
	}

	if detected.DetectedType == "" {
		fmt.Fprint(responseWriter, "No type provided!")
		return
	}
	var jsonData = jsonEncode(detected)
	message := &pubsub.Message{
		Data: jsonData,
	}
	var topic = getTopic(detected.DetectedType)
	id, err := client.Topic(topic).Publish(request.Context(), message).Get(request.Context())
	if err != nil {
		log.Printf("project(%s) topic(%s).Publish.Get: %v", projectID, HornetDetectedTopic, err)
		http.Error(responseWriter, "Error publishing message", http.StatusInternalServerError)
		return
	}
	fmt.Fprint(responseWriter, html.EscapeString(detected.DetectedType))
	fmt.Fprintf(responseWriter, "Message published: %v", id)
}
