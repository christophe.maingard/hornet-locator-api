module toche-hl/trigger_detect

go 1.13

require (
	cloud.google.com/go/pubsub v1.3.1
	github.com/GoogleCloudPlatform/functions-framework-go v1.2.0 // indirect
)
