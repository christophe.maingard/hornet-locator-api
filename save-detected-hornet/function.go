package save_detected_hornet

import (
	"context"
	"encoding/json"
	"io"
	"log"
)

type Detected struct {
	DetectedType string `json:"detectedType"`
	LatLng       struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"LatLng"`
}

// PubSubMessage is the payload of a Pub/Sub event.
type PubSubMessage struct {
	Data []byte `json:"data"`
}

func SaveDetectedHornet(ctx context.Context, message PubSubMessage) error {
	var detected = Detected{}

	if err := json.Unmarshal(message.Data, &detected); err != nil {
		switch err {
		case io.EOF:
			log.Fatalf("Nothing provided")
		default:
			log.Fatalf("json.Unmarshal! %v", err)
		}
	}
	log.Printf("Received %v", detected)
	return nil
}
